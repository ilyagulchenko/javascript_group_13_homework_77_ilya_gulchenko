import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ThreadsService} from '../../services/threads.service';
import {ThreadData} from '../../models/thread.model';

@Component({
  selector: 'app-thread-form',
  templateUrl: './thread-form.component.html',
  styleUrls: ['./thread-form.component.sass']
})
export class ThreadFormComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(private threadService: ThreadsService) { }

  ngOnInit(): void {
  }

  createThread() {
    const threadData: ThreadData = this.form.value;
    this.threadService.addThread(threadData).subscribe(() => {
      this.threadService.getThreads();
    });
  }

}
