import {Component, OnDestroy, OnInit} from '@angular/core';
import {Thread} from '../models/thread.model';
import {ThreadsService} from '../services/threads.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-threads',
  templateUrl: './threads.component.html',
  styleUrls: ['./threads.component.sass']
})
export class ThreadsComponent implements OnInit, OnDestroy {
  threadsChangeSubscription!: Subscription;

  threads: Thread[] = [];

  constructor(private threadService: ThreadsService) { }

  ngOnInit(): void {
    this.threadsChangeSubscription = this.threadService.threadsFetch.subscribe((threads: Thread[]) => {
      this.threads = threads;
    });
    this.threadService.getThreads();
  }

  ngOnDestroy() {
    this.threadsChangeSubscription.unsubscribe();
  }

}
