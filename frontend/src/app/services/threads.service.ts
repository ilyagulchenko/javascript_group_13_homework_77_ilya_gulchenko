import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Thread, ThreadData} from '../models/thread.model';
import {map, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThreadsService {
  threadsFetch = new Subject<Thread[]>();

  constructor(private http: HttpClient) { }

  private threads: Thread[] = [];

  getThreads() {
    return this.http.get<Thread[]>(environment.apiUrl + '/threads').pipe(
      map(response => {
        return response.map(threadData => {
          return new Thread(
            threadData.id,
            threadData.author,
            threadData.message,
            threadData.image,
            new Date(threadData.datetime).toLocaleString(),
          );
        });
      })
    ).subscribe(threads => {
      this.threads = threads;
      this.threadsFetch.next(this.threads);
    })
  }

  addThread(threadData: ThreadData) {
    const formData = new FormData();

    Object.keys(threadData).forEach(key => {
      if (threadData[key] !== null) {
        formData.append(key, threadData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/threads', formData);
  }
}
