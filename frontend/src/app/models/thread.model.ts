export class Thread {
  constructor(
    public id: string,
    public author: string,
    public message: string,
    public image: string,
    public datetime: string,
  ) { }
}

export interface ThreadData {
  [key: string]: any;
  author: string;
  message: number;
  image: File | null;
}
