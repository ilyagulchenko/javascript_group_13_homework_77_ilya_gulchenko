import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ThreadsComponent } from './threads/threads.component';
import { ThreadFormComponent } from './threads/thread-form/thread-form.component';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {TextFieldModule} from '@angular/cdk/text-field';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule} from '@angular/forms';
import {LayoutModule} from '@angular/cdk/layout';
import {FileInputComponent} from './ui/file-input/file-input.component';
import {HttpClientModule} from '@angular/common/http';
import {AuthorPipe} from './pipes/author.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ThreadsComponent,
    ThreadFormComponent,
    FileInputComponent,
    AuthorPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexModule,
    FlexLayoutModule,
    LayoutModule,
    MatCardModule,
    MatButtonModule,
    TextFieldModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
