const fs = require('fs').promises;
const {nanoid} = require("nanoid");

const fileName = './db.json';
let data = [];

module.exports = {
    async init() {
        try {
            const fileContents = await fs.readFile(fileName);
            data = JSON.parse(fileContents.toString());
        } catch (e) {
            data = [];
        }
    },
    getThreads() {
        return data;
    },
    addThread(thread) {
        thread.id = nanoid();
        thread.datetime = new Date().toISOString();
        data.push(thread);
        return this.save();
    },
    save() {
        return fs.writeFile(fileName, JSON.stringify(data, null, 2));
    }
}
