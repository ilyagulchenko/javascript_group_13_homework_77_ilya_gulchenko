const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../fileDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', (req,res) => {
    const threads = db.getThreads();
    return res.send(threads);
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        const thread = {
            author: req.body.author,
            message: req.body.message,
        };

        if (req.file) {
            thread.image = req.file.filename;
        }

        if (!thread.message) {
            return res.status(400).send({error: 'Message must be present in the request'});
        }

        await db.addThread(thread);

        return res.send({message: 'Created new thread'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
